import React, { Component } from 'react';

import {
  AppRegistry,
  ActivityIndicator,
  StyleSheet,
  WebView,
  Text,
  Image,
  ListView,
  TouchableHighlight,
  Navigator,
  View
} from 'react-native';

import { WilIndex, WilDetail } from './screens';

export default class App extends Component {

  renderScene(route, navigator) {
    let Component;

    if (route.id === 'Index') Component = WilIndex;
    if (route.id === 'Detail') Component = WilDetail;

    return <Component route={route} navigator={navigator} />
  }

  handleNavigatorTitle(route, navigator, index, navState) {
    return (
      <Text style={{
        marginTop: 10, 
        fontWeight: 'bold', 
        fontSize: 15,
        color: '#fff'
      }}>{route.title}</Text>
    ); 

  }

  handleNavigatorLeftButton(route, navigator, index, navState) {
    if (route.id === 'Detail') {
      return (
        <TouchableHighlight style={{marginTop: 10, marginLeft: 5, width: 20, height: 20}} onPress={() => navigator.pop()}>
          <Image style={{width: 20, height: 20}} source={require('./screens/Detail/assets/back.png')} />
        </TouchableHighlight>
      )
    }

    return null;
  }

  handleNavigatorRightButton(route, navigator, index, navState) {
    return null;
  }

  render() {
    return (
      <Navigator
        initialRoute={{ id: 'Index', title: 'Правила Жизни', index: 0 }}
        renderScene={(route, navigator) => {  
          return this.renderScene(route, navigator)
        }}
        navigationBar={
          <Navigator.NavigationBar
            routeMapper={{
              LeftButton: this.handleNavigatorLeftButton,
              RightButton: this.handleNavigatorRightButton,
              Title: this.handleNavigatorTitle
            }}
            style={{
              backgroundColor: '#000'
            }}
          />
        }
      />
    )
  }
}


AppRegistry.registerComponent('WilApp', () => App);
