import React, { Component } from 'react';

import {
  TouchableHighlight,
  View,
  Text,
  Image
} from 'react-native'

import styles from './styles'

export default class Row extends Component {

  handlePress() {
    const { navigator } = this.props;
    const [title, imageSource, seriesName, id, rating] = this.props.data;

    navigator.push({id: 'Detail', 'post_id': id, title });
  }


  render() {
    const [title, imageSource, seriesName, id, rating] = this.props.data;

    return (
      <TouchableHighlight onPress={this.handlePress.bind(this, title)}>
        <View style={styles.card}>
          <View style={styles.mask} />
          <Image style={styles.photo} source={{uri: imageSource}} />
          <Text style={styles.name}>{title}</Text>
          <Text style={styles.seriesName}>{seriesName.toUpperCase()}</Text>
          {/*
            <Text style={styles.rating}>{'➦ ' + rating}</Text>
          */}
        </View>
      </TouchableHighlight>
    );

  }

}