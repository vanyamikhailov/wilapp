import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  name: {
    position: 'absolute',
    zIndex: 3,
    left: 0, 
    right: 0, 
    bottom: 50, 
    backgroundColor: 'rgba(0, 0, 0, 0)', 
    color: '#fff', 
    fontSize: 30, 
    fontWeight: 'bold',
    textAlign: 'center', 
  },
  seriesName: {
    position: 'absolute',
    zIndex: 3,
    left: 0, 
    right: 0, 
    bottom: 25, 
    backgroundColor: 'rgba(0, 0, 0, 0)', 
    color: '#fff', 
    fontSize: 10, 
    fontWeight: 'bold',
    textAlign: 'center',
    letterSpacing: 2
  },
  rating: {
    position: 'absolute',
    zIndex: 3,
    right: 10, 
    top: 25, 
    backgroundColor: 'rgba(0, 0, 0, 0)', 
    color: '#fff', 
    fontSize: 13, 
    fontWeight: 'bold',
    textAlign: 'left',
    letterSpacing: 1
  },
  card: {
    height: 300,
  },
  mask: {
    height: 300,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 2
  },
  photo: {
    height: 300,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1
  }
})