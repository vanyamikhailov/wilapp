import React, { Component } from 'react'
import { 
    View
} from 'react-native';

import styles from './styles'

export default class RowSeparator extends Component {
    render() {
        const { sectionId, rowId } = this.props;

        return (
            <View 
                key={rowId}
                style={styles.container} 
            />
        )
    }
}
