import React, { Component } from 'react';

import {
  ActivityIndicator,
  StatusBar,
  ListView,
  View
} from 'react-native';

import { Row, RowSeparator } from '../../components';
import styles from './styles';

export default class WilIndex extends Component {

  state = {
    data: [],
    ds: null
  }

  componentDidMount() {
    fetch('https://media.esquire.ru/api/wil/?page=1&page_size=30')
    .then((r) => r.json())
    .then((rData) => rData)
    .then((data) => {

      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      const array = [];
      const posts = data.results.map((value) => value.post);

      posts.forEach((p, k) => {
        const image = p.images.length ? p.images[0].url_2x : p.story_image.url_2x;
        if (k > 1) {
          array.push([p.title, image, (p.series ? p.series.name : ''), p.id, p.rating_values.facebook + p.rating_values.vk]);
        }
      })

      this.setState({
        ds: ds.cloneWithRows(array),
      })
    })
  }

  render() {
    const { ds } = this.state;

    return (
      <View style={styles.container}>
        <StatusBar
          // backgroundColor="blue"
          barStyle="light-content"
        />
        {
          ds === null ?
          <ActivityIndicator
            animating
            style={styles.indicator}
            size="large"
          /> :
          <ListView
            style={styles.list}
            dataSource={this.state.ds}
            renderSeparator={
              (sectionId, rowId) => <RowSeparator key={rowId} sectionId={sectionId} rowId={rowId} />
            }
            renderRow={
              (data, rowId) => <Row key={rowId} data={data} navigator={this.props.navigator} />
            }
          />
        }
      </View>
    );
  }
}