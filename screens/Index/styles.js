import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  indicator: {
    height: 80, 
    marginTop: 80
  },
  list: {
    flex: 1,
    marginTop: 60,
    backgroundColor: '#000'
  }
});