import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'container': {
    flex: 1,
    backgroundColor: '#fff',
  },
  'indicator': {
    height: 80, 
    marginTop: 80
  },
  'browser': {
    marginTop: 70,
  }
})
