import React, { Component } from 'react';

import {
  ActivityIndicator,
  Dimensions,
  Image,
  WebView,
  ScrollView,
  View
} from 'react-native';

import styles from './styles';

export default class WilDetail extends Component {

  state = {
    data: null,
    webViewHeight: 0
  }

  componentDidMount() {
    fetch(`https://media.esquire.ru/api/post/${this.props.route.post_id}`)
    .then((r) => r.json())
    .then((rData) => rData)
    .then((data) => {
      this.setState({data})
    })
  }

  updateWebViewHeight(event) {
    //jsEvaluationValue contains result of injected JS
    this.setState({webViewHeight: parseInt(event.jsEvaluationValue)});
  }

  render() {
    const { data } = this.state;

    return (
      <View style={styles.container}>

        { data === null ?
          <ActivityIndicator
            animating
            style={styles.indicator}
            size="large"
          /> :
          <ScrollView style={{marginTop: 64}}>
            <Image 
              source={{uri: data.og_image.url_2x}} 
              style={{height: 300, backgroundColor: 'rgba(0,0,0,0.1)'}} 
            />
            <WebView 
              style={{
                width: Dimensions.get('window').width, 
                height: this.state.webViewHeight,
                marginTop: 10
              }}
              injectedJavaScript="document.body.scrollHeight;"
              onNavigationStateChange={this.updateWebViewHeight.bind(this)}
              automaticallyAdjustContentInsets={true} 
              source={{html: `<html><body>${data.rendered_text_mobile}</body></html>`}} />
          </ScrollView>
        }
      </View>
    )
  }
}
